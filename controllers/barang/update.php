<?php 
include "../../class/Barang.php";
$barang = new Barang();

	//mengisi attribute dengan hasil imputan 
$barang->bar_id = $_POST['bar_id'];
$barang->bar_nama = $_POST['bar_nama'];

$barang->bar_hargaJual = $_POST['bar_hargaJual'];
$barang->bar_jenis = $_POST['bar_jenis'];

$gambar = $_FILES['upload_foto']['name'];

if(empty($gambar)){
	$error = $barang->update_tidakPakeFoto();
}else{
	$hapus = $barang->hapus_gambar($_POST['bar_id']);

	$lokasi = $hapus['bar_foto'];

	$hapus_gambar="../../res/foto_produk/$lokasi";

    // script untuk menghapus gambar dari folder
	unlink($hapus_gambar);

	$gambar = $_FILES['upload_foto']['name'];
	$tmp = $_FILES['upload_foto']['tmp_name'];

	$fotobaru = $_POST['bar_nama']."_".date('dmYHis')."_".$gambar;

	$path = "../../res/foto_produk/".$fotobaru;

	$barang->bar_foto = $fotobaru;
    move_uploaded_file($tmp,$path);


	$error = $barang->update_PakeFoto();
}	




	//menampung hasil dari method update

	//pengecekan error atau berhasil, !$error = berhasil
if(!$error){
		//memanggil tampilan detail dengan mengirimkan page dan nrp
	header("location: ../../pages/admin/index.php?page=detail_barang&bar_id={$barang->bar_id}"); 
} else {
		//membuat session untuk menampilkan pesan error bernama message
	session_start();
	$_SESSION['message'] = $error;
		//memanggil tampilan update kembali
	header("location: ../../pages/admin/index.php?page=tabel_barang");
}

?>