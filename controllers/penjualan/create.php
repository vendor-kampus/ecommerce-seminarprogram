<?php 
if(!isset($_SESSION)) 
{ 
	session_start(); 
}  

include "../../class/Barang.php";
$barang = new Barang();
include "../../class/Penjualan.php";
$penjualan = new Penjualan();

include "../../class/Detail_Penjualan.php";
$detail_penjualan = new Detail_Penjualan();

// echo "<pre>";
// print_r($_SESSION);
// echo "</pre>";


$penjualan->penj_noTransaksi = $_POST['penj_noTransaksi'];
$penjualan->pel_nama = $_POST['pel_nama'];
$penjualan->pel_alamat = $_POST['pel_alamat'];
$penjualan->pel_jenisKelamin = $_POST['pel_jenisKelamin'];
$penjualan->pel_kota = $_POST['pel_kota'];
$penjualan->pelnoHp = $_POST['pel_noHp'];

$penjualan->penj_totalBayar = $_POST['penj_totalBayar'];

$error = $penjualan->create();

foreach ($_SESSION['keranjang'] as $id_barang => $jumlah) {
	$dataBarang = $barang->getDetail($id_barang);

	$detail_penjualan->penj_noTransaksi = $_POST['penj_noTransaksi'];
	$detail_penjualan->bar_id = $dataBarang['bar_id'];
	$detail_penjualan->jumlah_beli = $jumlah;
	$detail_penjualan->bar_hargaJual = $dataBarang['bar_hargaJual'];
	$detail_penjualan->subtotal_harga = $dataBarang['bar_hargaJual']*$jumlah;
	$detail_penjualan->keterangan = $_SESSION['keterangan'][$id_barang];	
	$error = $detail_penjualan->create();
}

if(!$error){
	unset($_SESSION['keterangan']);
	unset($_SESSION['keranjang']);
	// unset($_SESSION['Tanggal_Transaksi']);
	header("location: ../..//index.php");
} else {                
	echo "Input Gagal banget";
}


?>

