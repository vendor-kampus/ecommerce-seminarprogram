<?php 
include '../../class/Penjualan.php';
$penjualan = new Penjualan();

// $dataTerakhir = $->getDataTerakhir();
$bulan = date('m');

$dataPendapatanBulan = $penjualan->getDetail_PendapatanPerBulan($bulan);
$dataTransaksiBulan = $penjualan->getDetail_jumlahTransaksi($bulan);


?>
<div>
  <div class="row">
  <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Pendapatan Bulan (<?= date('M')  ?>)</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">Rp. <?= number_format($dataPendapatanBulan['total_transaksi']); ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Pesanan Bulan (<?= date('M'); ?>)</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $dataTransaksiBulan['total_transaksi'] ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-comments fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
 







<div class="table-responsive m-b-40">
  <table class="table table-borderless table-data3">
    <thead>
      <tr>
        <th>Tanggal</th>
        <th>Nama</th>
        <th>Alamat</th>
        <th>Kota</th>
        <th>Total Pembelian</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($penjualan->getDataTerakhir() as $no =>$data) : ?>
       <tr>
         <td><?= $data['created_at'] ?></td>
         <td><?= $data['pel_nama'] ?></td>
         <td><?= $data['pel_alamat'] ?></td>
         <td><?= $data['pel_kota'] ?></td>
         <td>Rp. <?= number_format($data['penj_totalBayar']) ?></td>
       </tr>
     <?php endforeach ?> 
   </tbody>
 </table>
</div>

<!-- <div class="row">
  <div class="col-md-4">
    <div class="chart-percent-3 m-b-40">
      <h3 class="title-3 m-b-25">chart by %</h3>
      <div class="chart-note m-b-5">
        <span class="dot dot--blue"></span>
        <span>Penjualan</span>
      </div>
      <div class="chart-note">
        <span class="dot dot--red"></span>
        <span>Barang</span>
      </div>
      <div class="chart-wrap m-t-60">
        <canvas id="percent-chart2"></canvas>
      </div>
    </div>
  </div>
</div>
   -->