	<?php
	// include '../../class/Administrator.php';
	// $admin = new Admin();

	?>
	<?php if($page=="tabel_login") : ?>

		<table id="tabel_login" class="table align-items-center table-flush" style="width: 100%;">
			<thead class="thead-dark">
				<tr>
					<th style="font-size: 17px;"><center>No</center></th>
					<th style="font-size: 17px;"><center>Nama Admin</center></th>
					<!-- <th style="font-size: 17px;"><center>id museum</center></th> -->
					<th style="font-size: 17px;"><center>Email</center></th>
					<th style="font-size: 17px;"><center>Username</center></th>
					<th style="font-size: 17px;"><center>created</center></th>

					<th style="font-size: 17px;"><center>Aksi</center></th>

				</tr>
			</thead>
			<tbody>
				<?php foreach($admin_class->getData() as $no =>$data) : ?>

					<tr>
						<td><center><?= $no+1; ?></center></td>
						<td><?= $data['adm_nama'] ?></td>
						<td><?= $data['adm_email'] ?></td>
						<td><?= $data['adm_username'] ?></td>
						<td><?= $data['created_at'] ?></td>
						<td>
							<a href="?page=detail_admin_cek&adm_id=<?= $data['adm_id']; ?>" class="btn btn-info">Info</a>
							<!-- <a href="?page=update_admin&adm_id=<?= $data['adm_id']; ?>" class="btn btn-success">Ubah</a>
							<a href="" class="btn btn-danger">Delete</a> -->

						</td>

					</tr>
				<?php endforeach ?>

			</tbody>
		</table>
	<?php endif; ?>

	<?php if($page=="tabel_barang") : ?>
		<?php
		include '../../class/Barang.php';
		$barang = new Barang();
		?><br>
		<center>
		<h1>Tabel Barang</h1>

		</center>
		<a href="?page=tambah_barang" class="btn btn-success">Tambah Barang</a>
		<hr>
		<table id="tabel_login" class="table align-items-center" style="width: 100%;">
			<thead class="thead-dark">
				<tr>
					<th style="font-size: 17px;"><center>No</center></th>
					<th style="font-size: 17px;"><center>Nama Barang</center></th>
					<!-- <th style="font-size: 17px;"><center>id museum</center></th> -->
					<th style="font-size: 17px;"><center>Harga Jual</center></th>
					<th style="font-size: 17px;"><center>Jenis</center></th>
					<th style="font-size: 17px;"><center>Aksi</center></th>

				</tr>
			</thead>
			<tbody>
				<?php foreach($barang->getData() as $no =>$data) : ?>

					<tr>
						<td><center><?= $no+1; ?></center></td>
						<td class="text-center"><?= $data['bar_nama'] ?></td>
						<td class="text-center">Rp. <?= number_format($data['bar_hargaJual']) ?></td>
						<td class="text-center"><?= $data['bar_jenis'] ?></td>
						<td class="text-center">
							<a href="?page=detail_barang&bar_id=<?= $data['bar_id']; ?>" class="btn btn-info">Info</a>
							<a href="?page=update_barang&bar_id=<?= $data['bar_id']; ?>" class="btn btn-success">Ubah</a>
							<a href="?page=delete_barang&bar_id=<?= $data['bar_id']; ?>" class="btn btn-danger">Delete</a>

						</td>

					</tr>
				<?php endforeach ?>

			</tbody>
		</table>
	<?php endif; ?>

	<?php if($page=="tabel_penjualan") : ?>
		<h1>Tabel Penjualan</h1>
		<?php
		include '../../class/Penjualan.php';
		$penjualan = new Penjualan();
		?>

		<table id="tabel_login" class="table align-items-center table-flush" style="width: 100%;">
			<thead class="thead-dark">
				<tr>
					<th style="font-size: 17px;"><center>No</center></th>
					<th style="font-size: 17px;"><center>Nama Pelanggan</center></th>
					<!-- <th style="font-size: 17px;"><center>id museum</center></th> -->
					<th style="font-size: 17px;"><center>Alamat</center></th>
					<th style="font-size: 17px;"><center>No HP</center></th>
					<th style="font-size: 17px;"><center>Aksi</center></th>

				</tr>
			</thead>
			<tbody>
				<?php foreach($penjualan->getData() as $no =>$data) : ?>

					<tr>
						<td><center><?= $no+1; ?></center></td>
						<td class="text-center"><?= $data['pel_nama'] ?></td>
						<td class="text-center"><?= $data['pel_alamat'] ?></td>
						<td class="text-center"><?= $data['pel_noHp'] ?></td>
						<td class="text-center">
							<a href="?page=detail_penjualan&penj_noTransaksi=<?= $data['penj_noTransaksi']; ?>" class="btn btn-info">Info</a>
							<!-- <a href="?page=update_barang&bar_id=<?= $data['bar_id']; ?>" class="btn btn-success">Ubah</a>
							<a href="" class="btn btn-danger">Delete</a> -->

						</td>

					</tr>
				<?php endforeach ?>

			</tbody>
		</table>
	<?php endif; ?>
