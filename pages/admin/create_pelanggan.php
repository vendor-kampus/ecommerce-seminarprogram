<?php

require_once "class/Penjualan.php";
$penjualan = new Penjualan();
$tahun = date('Y');
$kd = $tahun;
$id=0;
foreach ($penjualan->getData() as $data ) {
	if (((int)(substr($data['penj_noTransaksi'], 4))) >= $id) {
		$id = ((int)(substr($data['penj_noTransaksi'], 4)));
	}
}
$id++;
$id_otomatis = $kd."".$id;


?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8" id="signup_msg">
			<!--Alert from signup form-->
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="panel panel-primary">
				<div class="panel-heading">Customer Form</div>
				<div class="panel-body">

					<form action="controllers/penjualan/create.php" method="POST">
						<div class="row">
							<div class="col-md-6">
								<label for="f_name">No Transaksi</label>
								<input type="text" name="penj_noTransaksi" class="form-control" value="<?= $id_otomatis ?>" readonly>
							</div>
							<div class="col-md-6">
								<label for="f_name">Nama Lengkap</label>
								<input type="text" name="pel_nama"class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label for="email">Alamat Lengkap</label>
								<input type="text" name="pel_alamat" class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label for="mobile">Phone</label>
								<input type="number" name="pel_noHp" class="form-control" maxlength="12">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label for="address1">Jenis Kelamin</label>
								<select class="form-control" name="pel_jenisKelamin" style="">
									<option value="Perempuan">Perempuan</option>
									<option value="Laki-Laki">Laki-Laki</option>
								</select>
								<input type="hidden" name="penj_totalBayar" value="<?= $subtotal ?>" class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label for="address2">Kota</label>
								<input type="text" name="pel_kota" class="form-control">
								<!-- <input type="hidden" name="sutotal" class="form-control" value="<?= $subharga ?>"> -->
							</div>
						</div>
						<p><br/></p>
						<div class="row">
							<div class="col-md-12">
								<input style="width:100%;" value="Simpan" type="submit" name="signup_button"class="btn btn-success btn-lg">
							</div>
						</div>

					</div>
				</form>
				<div class="panel-footer"></div>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
