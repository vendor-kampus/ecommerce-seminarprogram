<?php if($page=="detail_barang") : ?>
	<?php
		//sama seperti tampilan detail, untuk membuat form update
		// diperlukan 1 data dari student
	include '../../class/Barang.php';
	$barang = new Barang();
	$data = null;
	if(isset($_GET['bar_id'])){
			//mengambil semua data berdasarkan nrp
		$data = $barang->getDetail($_GET['bar_id']);
	}
	?>

	<p>
		<div class="row">
			<div class="col-md-9">
				<h3> Detail Barang </h3>
			</div>
			<div class="col-md-3">
				<a href="index.php?page=tabel_barang" class="btn btn-success"> Kembali </a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<center>
					<img src="../../res/foto_produk/<?= $data['bar_foto'] ?>" style="width: 200px; height: 250px;" />
				</center>
			</div>
		</div>


	<!-- <a href="index.php?page=student-create" class="btn btn-success"> Tambah </a>
		<a href="index.php?page=student" class="btn btn-success"> Kembali </a> -->
	</p>
	<br>
	<P>
		<?php if($data) : ?>
			<table class="table table-hover table-white" >
				<tr>
					<th class="text-left"> Id Barang </th>
					<td class="text-right"> <?= $data['bar_id'] ?> </td>
				</tr>
				<tr>
					<th class="text-left"> Nama </th>
					<td class="text-right"> <?= $data['bar_nama'] ?> </td>
				</tr>
				<tr>
					<th class="text-left"> Jenis </th>
					<td class="text-right"> <?= $data['bar_jenis'] ?> </td>
				</tr>
				<tr>
					<th class="text-left"> Harga Jual </th>
					<td class="text-right">Rp. <?= number_format($data['bar_hargaJual']) ?> </td>
				</tr>
			</table>
		<?php endif; ?>
	</p>
	<?php endif; ?>


	<?php if($page=="detail_admin") : ?>
		<?php
		// include '../../class/Administrator.php';
		$admin = new Admin();
		$data = null;
		if(isset($_GET['adm_id'])){
				//mengambil semua data berdasarkan nrp
			$data = $admin->getDetail($_GET['adm_id']);
		}

		?>

		<p>
			<div class="row">
				<div class="col-md-9">
					<h3> Detail Admin </h3>
				</div>
				<div class="col-md-3">
					<a href="?page=update_admin&adm_id=<?= $data['adm_id']; ?>" class="btn btn-outline-success btn-block"> Perbaharui </a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<center>
						<img src="../../res/foto_admin/<?= $data['adm_foto'] ?>" style="width: 200px; height: 250px;" />
					</center>
				</div>
			</div>


		<!-- <a href="index.php?page=student-create" class="btn btn-success"> Tambah </a>
			<a href="index.php?page=student" class="btn btn-success"> Kembali </a> -->
		</p>
		<br>
		<P>
			<?php if($data) : ?>
				<table class="table table-hover table-white" >
					<tr>
						<th class="text-left"> Id Admin </th>
						<td class="text-right"> <?= $data['adm_id'] ?> </td>
					</tr>
					<tr>
						<th class="text-left"> Nama  </th>
						<td class="text-right"> <?= $data['adm_nama'] ?> </td>
					</tr>
					<tr>
						<th class="text-left"> Email </th>
						<td class="text-right"> <?= $data['adm_email'] ?> </td>
					</tr>
					<tr>
						<th class="text-left"> Dibuat sejak </th>
						<td class="text-right"> <?= $data['created_at'] ?> </td>
					</tr>
					<tr>
						<th class="text-left"> Terakhir di update </th>
						<td class="text-right"> <?= $data['update_at'] ?> </td>
					</tr>
				</table>
			<?php endif; ?>
		</p>
		<?php endif; ?>

	<?php if($page=="detail_admin_cek") : ?>
		<?php
		// include '../../class/Administrator.php';
		$admin = new Admin();
		$data = null;
		if(isset($_GET['adm_id'])){
				//mengambil semua data berdasarkan nrp
			$data = $admin->getDetail($_GET['adm_id']);
		}

		?>

		<p>
			<div class="row">
				<div class="col-md-9">
					<h3> Detail Admin </h3>
				</div>
				<div class="col-md-3">
					<a href="?page=tabel_login" class="btn btn-outline-success btn-block"> Kembali </a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<center>
						<img src="../../res/foto_admin/<?= $data['adm_foto'] ?>" style="width: 200px; height: 250px;" />
					</center>
				</div>
			</div>


		<!-- <a href="index.php?page=student-create" class="btn btn-success"> Tambah </a>
			<a href="index.php?page=student" class="btn btn-success"> Kembali </a> -->
		</p>
		<br>
		<P>
			<?php if($data) : ?>
				<table class="table table-hover table-white" >
					<tr>
						<th class="text-left"> Id Admin </th>
						<td class="text-right"> <?= $data['adm_id'] ?> </td>
					</tr>
					<tr>
						<th class="text-left"> Nama  </th>
						<td class="text-right"> <?= $data['adm_nama'] ?> </td>
					</tr>
					<tr>
						<th class="text-left"> Email </th>
						<td class="text-right"> <?= $data['adm_email'] ?> </td>
					</tr>
					<tr>
						<th class="text-left"> Dibuat sejak </th>
						<td class="text-right"> <?= $data['created_at'] ?> </td>
					</tr>
					<tr>
						<th class="text-left"> Terakhir di update </th>
						<td class="text-right"> <?= $data['update_at'] ?> </td>
					</tr>
				</table>
			<?php endif; ?>
		</p>
		<?php endif; ?>


	<?php if($page=="detail_penjualan") : ?>
		<?php

		include '../../class/penjualan.php';
		$penjualan = new Penjualan();
		if(isset($_GET['penj_noTransaksi'])){
			$dataPenjualan = $penjualan->getDetailPenjualan_($_GET['penj_noTransaksi']);
			$dataDetailPenjualan = $penjualan->getDetailPenjualan_barang($_GET['penj_noTransaksi']);
		}
		// print_r($dataDetailPenjualan);
		// die();
		?>

	<section class="container">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Detail Data Penjualan</h4>
		</div>
		<div class="card-body">
				<div class="col-md-12 mt-5 mx-auto">
					<div class="row">
						<div class="col-md-6">
						
							<address>
								<strong>
								Nomer Transaksi : </strong><?= $dataPenjualan['penj_noTransaksi'] ?>
								<br>
								Nama Pelanggan : <?= $dataPenjualan['pel_nama'] ?>
								<br>
								Alamat : <?= $dataPenjualan['pel_alamat'] ?>
								<br>
								Jenis Kelamin : <?= $dataPenjualan['pel_jenisKelamin'] ?>
								<br>
							</address>
						</div>
						<div class="col-md-6 text-right">
							<p>
								<em><?php
								$tgl= $dataPenjualan['created_at'];
								echo $tgl = date('d-m-Y');
								echo "<br>";
								echo $tgl = date('H:i:s');

								?></em>
							</p>
							<p>
								<!-- <em>No Transaksi: <?= $dataPenjualan['id_pelanggar'] ?></em> -->
							</p>
							<p>
								Kota : <?= $dataPenjualan['pel_kota'] ?> 
							</p>
						</div>
					</div>
					
					<div class="row">
						<div class="text-left">
							<h3> Rincian</h3>
						</div>
						<table class="table table-hover table-bordered">
							<thead>
								<tr>
									<th width="450px" scope="col">Nama Barang</th>
									<th width="150px" scope="col" class="text-center">Jumlah beli</th>

									<th width="150px" scope="col" class="text-center">Harga</th>
									<th width="200px" scope="col" class="text-center">Total</th>
								</tr>

							</thead>
							<tbody>
								<?php foreach ($dataDetailPenjualan as $penj_noTransaksi => $jumlah): ?> 
									<?php 
									$dataBarang = $penjualan->getDetail_barang($jumlah['bar_id']); 
										// print_r($dataPasal);
									?>
									<tr scope="row">
										<td >- <?= $dataBarang['bar_nama'] ?></td>
										<td class="text-center"><?= number_format($jumlah['jumlah_beli']); ?></td> 
										<td class="text-left">Rp. <?= number_format($dataBarang['bar_hargaJual']); ?> </td>
										<td class="text-left">Rp. <?= number_format($jumlah['jumlah_beli'] * $dataBarang['bar_hargaJual'])  ?></td>
									</tr>
							
								<?php endforeach ?>
								<tr>
									<td></td>
									<td></td>
									<td class="text-right"><h4><strong>Subtotal : </strong></h4></td>
									<td class="text-left text-danger"><h4><strong>Rp. <?= number_format($dataPenjualan['penj_totalBayar']); ?>,-</strong></h4></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<br>
				<div class="text-right">
					<a href="?page=tabel_penjualan" class="btn btn-success">Kembali</a>
						</div>
				</div>
			</div>
			</section>
			<?php endif; ?>
