Y<?php
if(!isset($_SESSION))
{
	session_start();
}
include "class/Barang.php";
$barang = new Barang();
$konsole = 0;
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>E-Commerce</title>
		<link rel="stylesheet" href="Style/tambahan/css/bootstrap.min.css"/>
		<script src="Style/tambahan/js/jquery2.js"></script>
		<script src="Style/tambahan/js/bootstrap.min.js"></script>
		<!-- <script src="Style/tambahan/main.js"></script> -->
		<link rel="stylesheet" type="text/css" href="Style/tambahan/style.css">
		<style></style>
	</head>
<body style="background-color: #white;">
<!-- <div class="wait overlay">
	<div class="loader"></div>
</div> -->

	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse" aria-expanded="false">
					<span class="sr-only">navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="#" class="navbar-brand">E-Commerce</a>
			</div>
		<div class="collapse navbar-collapse" id="collapse">
			<ul class="nav navbar-nav">
				<!-- <li><a href="index.php"><span class="glyphicon glyphicon-home"></span>Home</a></li> -->
				<li><a href="index-tran.php"><span class="glyphicon glyphicon-modal-window"></span>Product</a></li>
			</ul>
			<form class="navbar-form navbar-left">
		        <div class="form-group">
		          <input type="text" class="form-control" placeholder="Search" id="search">
		        </div>
		        <button type="submit" class="btn btn-primary" id="search_btn"><span class="glyphicon glyphicon-search"></span></button>
		     </form>
				 <ul class="nav navbar-nav navbar-right">
					 <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-shopping-cart"></span>Cart<span class="badge">
						 <?php
						 	$ker = count($_SESSION['keranjang']);
							echo $ker;
						 ?>
					 </span></a>
					<div class="dropdown-menu" style="width:400px;">
						<div class="panel panel-success">
							<div class="panel-heading">
								<div class="row">
									<div class="col-md-3">No.</div>
									<div class="col-md-3">Product Image</div>
									<div class="col-md-3">Product Name</div>
									<div class="col-md-3">Price in Rp.</div>
								</div>
							</div>
							<div class="panel-body">
								<?php foreach ($_SESSION['keranjang'] as $id_barang => $jumlah): ?>
									<?php
									$dataBarang = $barang->getDetail($id_barang);

									$konsole++;
									?>
									<div class="row">
										<div class="col-md-3"><?= $konsole; ?></div>
										<div class="col-md-3"><img src="res/foto_produk/<?= $dataBarang['bar_foto'] ?>" width="50" height="45" /></div>
										<div class="col-md-3"><?= $dataBarang['bar_nama']; ?></div>
										<div class="col-md-3"><?= number_format($dataBarang['bar_hargaJual']) ?></div>
									</div>
									<br>
								<?php endforeach; ?>
								<a href="detail-tran.php" class="btn btn-warning" style="float: right;">Edit</a>
							</div>
							<div class="panel-footer"></div>
						</div>
					</div>
				</li>
<!-- 				<li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>Login</a>
					<ul class="dropdown-menu">
						<div style="width:300px;">
							<div class="panel panel-primary">
								<div class="panel-heading">Login</div>
								<div class="panel-heading">
									<form onsubmit="return false" id="login">
										<label for="email">Email</label>
										<input type="email" class="form-control" name="email" id="email" required/>
										<label for="email">Password</label>
										<input type="password" class="form-control" name="password" id="password" required/>
										<p><br/></p>
										<a href="#" style="color:white; list-style:none;">Forgotten Password</a><input type="submit" class="btn btn-success" style="float:right;">
									</form>
								</div>
								<div class="panel-footer" id="e_msg"></div>
							</div>
						</div>
					</ul>
				</li> -->
			</ul>
		</div>
	</div>
</div>
	<p><br/></p>
	<p><br/></p>
	<p><br/></p>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-2 col-xs-12">
				<!-- <div id="get_category">
				</div> -->
				<div class="nav nav-pills nav-stacked">
					<li class="active"><a href="#"><h4>Categories</h4></a></li>
					<li><a href="#">Buku</a></li>
					<li><a href="#">Baju</a></li>
				</div>
				<div id="get_brand">
				</div>
				<!--<div class="nav nav-pills nav-stacked">
					<li class="active"><a href="#"><h4>Brand</h4></a></li>
					<li><a href="#">Categories</a></li>
					<li><a href="#">Categories</a></li>
					<li><a href="#">Categories</a></li>
					<li><a href="#">Categories</a></li>
				</div> -->
			</div>
			<div class="col-md-8 col-xs-12">
				<div class="row">
					<div class="col-md-12 col-xs-12" id="product_msg">
					</div>
				</div>
				<div class="panel panel-info" style="background-color: #139f49;">
					<div class="panel-heading">Barang</div>
					<div class="panel-body">
						<!-- <div id="get_product"> -->
							<!--Here we get product jquery Ajax Request-->
						<!-- </div> -->
						<?php foreach($barang->getData() as $data) : ?>
						<?php
						// echo "<pre>";
						// print_r($data);
						// echo "</pre>";

						 ?>
						<div class="col-md-6" >
									<div class="panel panel-info" >
										<div class="panel-heading"><?= $data['bar_nama'] ?></div>
										<div class="panel-body">
											<img src="res/foto_produk/<?= $data['bar_foto'] ?>" width="200" height="300" class="col-md-10" />


										</div>
										<div>
											<form class="col-md-3"  style="float:right;" action="controllers/penjualan/session_penjualan.php" method="post">
												<input type="hidden"class="form-control" name="bar_id" value="<?= $data['bar_id'] ?>" />

												<input type="hidden" class="form-control" name="jumlah_beli" value="1" />
												<?php if($data['bar_jenis']== 'Baju'){ ?>
													<select class="form-control" name="keterangan" style="">
														<option value="S">S</option>
														<option value="M">M</option>
														<option value="M">L</option>
														<option value="M">XL</option>
													</select>
													<!-- <input type="text" class="form-control" name="keterangan" value="" /> -->
												<?php }else{ ?>
													<input type="hidden" class="form-control" name="keterangan" value="-" />
												<?php } ?>
											</div>
											<div class="panel-heading">Rp. <?= number_format($data['bar_hargaJual']) ?>
											<button style="float: right;" class="btn btn-danger btn-xs">AddToCart</button>
										</form>
									</div>
								</div>
							</div>
					<?php endforeach ?>
					</div>
					<div class="panel-footer">&copy; 2019</div>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>

</body>
</html>
<?php
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre>";
?>
