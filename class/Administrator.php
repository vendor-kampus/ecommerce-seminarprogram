<?php
if(!class_exists('Database')){
    require('Database.php');
}

class Admin{
    public $adm_id;
    public $adm_nama;
    public $adm_username;
    public $adm_password;
    public $adm_foto;

    public function getLogin(){
      $db = new Database();
      $dbConnect = $db->connect();
      $sql = "SELECT * FROM login WHERE adm_username='{$this->adm_username}' AND adm_password='{$this->adm_password}'";
      $data = $dbConnect->query($sql);
      $dbConnect = $db->close();
      return $data;
    }
    public function getData(){
      $db = new Database();
      $dbConnect = $db->connect();
      $sql = "SELECT * FROM login";
      $data = $dbConnect->query($sql);
      $dbConnect = $db->close();
      return $data;
    }

    public function getDetail($adm_id){
      $db = new Database();
      $dbConnect = $db->connect();
      $sql = "SELECT * FROM login WHERE adm_id = '{$adm_id}'";
      $data = $dbConnect->query($sql);
      $dbConnect = $db->close();
      return $data->fetch_array();
    }
    
    public function create(){
    $db = new Database();
      //membuka koneksi
    $dbConnect = $db->connect();

      //query menyimpan data
    $sql = "insert into login (adm_id,adm_nama,adm_username,adm_password,adm_foto) values ('{$this->adm_id}','{$this->adm_nama}','{$this->adm_username}','{$this->adm_password}','{$this->adm_foto}')";
      //esekusi query di atas
    $data = $dbConnect->query($sql);

      //menampung error query simpan data
    $error = $dbConnect->error;
      //menutup koneksi
    $dbConnect = $db->close();
      //menampilkan nilai error
    return $error;
  }

   public function update_PakeFoto_pakePassword(){
    $db = new Database();
    $dbConnect = $db->connect();
    $sql = "UPDATE login SET adm_nama = '{$this->adm_nama}', adm_username = '{$this->adm_username}', adm_password = '{$this->adm_password}' , adm_foto = '{$this->adm_foto}' where adm_id = '{$this->adm_id}'";
    $data = $dbConnect->query($sql);
    $error = $dbConnect->error;
    $dbConnect = $db->close();
    return $error;
  }
  public function update_PakeFoto_tampaPassword(){
    $db = new Database();
    $dbConnect = $db->connect();
    $sql = "UPDATE login SET adm_nama = '{$this->adm_nama}', adm_username = '{$this->adm_username}', adm_password = '{$this->adm_password}' , adm_foto = '{$this->adm_foto}' where adm_id = '{$this->adm_id}'";
    $data = $dbConnect->query($sql);
    $error = $dbConnect->error;
    $dbConnect = $db->close();
    return $error;
  }
  

  public function update_tidakPakeFoto_tampaPassword(){
    $db = new Database();
    $dbConnect = $db->connect();
    $sql = "UPDATE login SET adm_nama = '{$this->adm_nama}', adm_username = '{$this->adm_username}' where adm_id = '{$this->adm_id}'";
    $data = $dbConnect->query($sql);
    $error = $dbConnect->error;
    $dbConnect = $db->close();
    return $error;
  }
  public function update_tidakPakeFoto_pakePassword(){
    $db = new Database();
    $dbConnect = $db->connect();
    $sql = "UPDATE login SET adm_nama = '{$this->adm_nama}', adm_username = '{$this->adm_username}', adm_password = '{$this->adm_password}' where adm_id = '{$this->adm_id}'";
    $data = $dbConnect->query($sql);
    $error = $dbConnect->error;
    $dbConnect = $db->close();
    return $error;
  }
  
  public function hapus_gambar($adm_id){
      $db = new Database();
      $dbConnect = $db->connect();
      $sql = "Select * from login where adm_id = '{$adm_id}'";
      $data = $dbConnect->query($sql);
      $dbConnect = $db->close();
      return $data->fetch_array();
    }


  


}

?>
