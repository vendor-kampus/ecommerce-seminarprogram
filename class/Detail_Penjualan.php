<?php
if(!class_exists('Database')){
    require('Database.php');
}

class Detail_Penjualan{
    public $penj_noTransaksi;
    public $bar_id;
    public $jumlah_beli;
    public $bar_hargaJual;
    public $subtotal_harga;
    public $keterangan;


    public function getData(){
      $db = new Database();
      $dbConnect = $db->connect();
      $sql = "SELECT * FROM penjualan";
      $data = $dbConnect->query($sql);
      $dbConnect = $db->close();
      return $data;
    }
    
    public function create() {
		$db = new Database();
			//membuka koneksi
		$dbConnect = $db->connect();

			//query menyimpan data
		$sql = "INSERT INTO detail_penjualan
		(
		penj_noTransaksi,
		bar_id,
		jumlah_beli,
		bar_hargaJual,
		subtotal_harga,
		keterangan
		)
		VALUES
		(
		'{$this->penj_noTransaksi}',
		'{$this->bar_id}',
		'{$this->jumlah_beli}',
		'{$this->bar_hargaJual}',
		'{$this->subtotal_harga}',
		'{$this->keterangan}'
	    )";
				//eksekusi query di atas
		$data = $dbConnect->query($sql);

				//menampung error simpan data
		$error = $dbConnect->error;

				//menutup koneksi
		$dbConnect = $db->close();

				//mengembalikan nilai error
		return $error;

	  }





}

?>
