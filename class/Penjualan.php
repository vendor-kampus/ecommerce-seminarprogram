<?php
if(!class_exists('Database')){
    require('Database.php');
}
class Penjualan{
    public $penj_noTransaksi;
    public $pel_nama;
    public $pel_alamat;
    public $pel_jenisKelamin;
    public $pel_kota;
    public $penj_totalBayar;
    public $create_at;

    public function getData(){
      $db = new Database();
      $dbConnect = $db->connect();
      $sql = "SELECT * FROM penjualan";
      $data = $dbConnect->query($sql);
      $dbConnect = $db->close();
      return $data;
    }

    public function create() {
		$db = new Database();
			//membuka koneksi
		$dbConnect = $db->connect();

			//query menyimpan data
		$sql = "INSERT INTO Penjualan
		(
		penj_noTransaksi,
		pel_nama,
		pel_alamat,
		pel_jenisKelamin,
		pel_kota,
		penj_totalBayar
		)
		VALUES
		(
		'{$this->penj_noTransaksi}',
		'{$this->pel_nama}',
		'{$this->pel_alamat}',
		'{$this->pel_jenisKelamin}',
		'{$this->pel_kota}',
		'{$this->penj_totalBayar}'
	    )";
				//eksekusi query di atas
		$data = $dbConnect->query($sql);

				//menampung error simpan data
		$error = $dbConnect->error;

				//menutup koneksi
		$dbConnect = $db->close();

				//mengembalikan nilai error
		return $error;

	  }

	 public function getDataTerakhir(){
      $db = new Database();
      $dbConnect = $db->connect();
      $sql = "Select * from penjualan order by penj_noTransaksi desc limit 10";
      $data = $dbConnect->query($sql);
      $dbConnect = $db->close();
      return $data;
	}
	public function getDetailPenjualan_($penj_noTransaksi) {
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "select * from penjualan inner join detail_penjualan on penjualan.penj_noTransaksi = detail_penjualan.penj_noTransaksi where penjualan.penj_noTransaksi = '{$penj_noTransaksi}'";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data->fetch_array();
	}
	public function getDetailPenjualan_barang($penj_noTransaksi) {
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "select * from detail_penjualan where penj_noTransaksi = '{$penj_noTransaksi}'";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data;
	}
	public function getDetail_barang($bar_id){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "select * from barang where bar_id = '{$bar_id}'";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data->fetch_array();
		}

	public function getDetail_PendapatanPerBulan($bulan) {
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "SELECT SUM(penj_totalBayar) as total_transaksi FROM penjualan WHERE MONTH(created_at) = '{$bulan}'";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data->fetch_array();
	}	

	public function getDetail_jumlahTransaksi($bulan) {
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "SELECT count(penj_totalBayar) as total_transaksi FROM penjualan WHERE MONTH(created_at) = '{$bulan}'";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data->fetch_array();
	}
		



}

?>
