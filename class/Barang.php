<?php
if(!class_exists('Database')){
    require('Database.php');
}
 // require('Database.php');
class Barang{

    public $bar_id;
    public $bar_nama;
    public $bar_hargaJual;
    public $bar_jenis;
    public $bar_foto;

    public function getData(){
      $db = new Database();
      $dbConnect = $db->connect();
      $sql = "SELECT * FROM barang";
      $data = $dbConnect->query($sql);
      $dbConnect = $db->close();
      return $data;
    }

    public function create() {
		$db = new Database();
			//membuka koneksi
		$dbConnect = $db->connect();

			//query menyimpan data
		$sql = "INSERT INTO barang
		(
		bar_id,
		bar_nama,
		bar_hargaJual,
		bar_jenis,
		bar_foto
		)
		VALUES
		(
		'{$this->bar_id}',
		'{$this->bar_nama}',
		'{$this->bar_hargaJual}',
		'{$this->bar_jenis}',
		'{$this->bar_foto}'

    )";
			//eksekusi query di atas
	$data = $dbConnect->query($sql);

			//menampung error simpan data
	$error = $dbConnect->error;

			//menutup koneksi
	$dbConnect = $db->close();

			//mengembalikan nilai error
	return $error;

  }
  public function update_PakeFoto(){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "UPDATE barang SET bar_nama = '{$this->bar_nama}', bar_hargaJual = '{$this->bar_hargaJual}', bar_jenis = '{$this->bar_jenis}' , bar_foto = '{$this->bar_foto}' where bar_id = '{$this->bar_id}'";
		$data = $dbConnect->query($sql);
		$error = $dbConnect->error;
		$dbConnect = $db->close();
		return $error;
	}
	

  public function update_tidakPakeFoto(){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "UPDATE barang SET bar_nama = '{$this->bar_nama}', bar_hargaJual = '{$this->bar_hargaJual}', bar_jenis = '{$this->bar_jenis}' where bar_id = '{$this->bar_id}'";
		$data = $dbConnect->query($sql);
		$error = $dbConnect->error;
		$dbConnect = $db->close();
		return $error;
	}

	public function hapus_gambar($bar_id){
		$db = new Database();
		$dbConnect = $db->connect();
		$sql = "Select * from barang where bar_id = '{$bar_id}'";
		$data = $dbConnect->query($sql);
		$dbConnect = $db->close();
		return $data->fetch_array();
	}

  public function getDetail($bar_id){
	$db = new Database();
	$dbConnect = $db->connect();
	$sql = "select * from barang where bar_id = '{$bar_id}'";
	$data = $dbConnect->query($sql);
	$dbConnect = $db->close();
	return $data->fetch_array();
	}

  public function delete(){
  $db = new Database();
  $dbConnect = $db->connect();
  $sql = "delete from barang where bar_id = '{$this->bar_id}'";
  $data = $dbConnect->query($sql);
  $error = $dbConnect->error;
  $dbConnect = $db->close();
  return $error;
}






}

?>
